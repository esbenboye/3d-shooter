class spriteloader{
	constructor(sheet, func){
		this.original = loadImage(sheet, func);
		this.sprites = {};
	}

	loadSprite(name, x,y, width,height){
		this.sprites[name] = this.original.get(x,y,width,height);
	}

	getSprite(name){
		return this.sprites[name]||createImage(1,1);
	}


}