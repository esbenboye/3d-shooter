
var cam;
var levelgen;
var sl;
var loaded;
var sprites = [];
var spriteSheet;
var map;


function preload(){
  var dimension = 128;
  var rows = 6;
  var cols = 6;
  var idx = 1;
  spriteSheet = loadImage('../sprites/sprite.gif', function(){
    for(var y = 0;y<rows;y++){
      for(var x = 0;x<cols;x++){
        sprites[idx] = spriteSheet.get((x*dimension)+x,(y*dimension)+y, dimension, dimension);
        console.log(idx);
        idx++;
      }
    }
  });
  
}

function setup(){
  
  // TODO: Find a better way to store and draw maps
  levelgen = new levelgenerator(sprites);

  cam = new lookingCam();
  createCanvas(700,400, WEBGL);

  map = [];
  map[9] = [
    {x:0,y:0,z:5},
    {x:0,y:0,z:6},
    {x:0,y:0,z:8},
    {x:0,y:0,z:9},

    {x:1,y:0,z:5},
    {x:1,y:0,z:9},

    {x:2,y:0,z:5},
    {x:2,y:0,z:9},

    {x:3,y:0,z:5},
    {x:3,y:0,z:9},

    {x:4,y:0,z:5},
    {x:4,y:0,z:9},

    {x:5,y:0,z:5},
    {x:5,y:0,z:9},

    {x:6,y:0,z:5},
    {x:6,y:0,z:9},

    {x:7,y:0,z:5},
    {x:7,y:0,z:9},

    {x:8,y:0,z:5},
    {x:8,y:0,z:9},

    {x:9,y:0,z:5},
    {x:9,y:0,z:9},

    {x:10,y:0,z:5},
    {x:10,y:0,z:9},

    {x:11,y:0,z:5},
    {x:11,y:0,z:9}

  ];
}


function draw(){

  background(51);
  cam.update();
  
  levelgen.draw(map);
}


function keyPressed(){
  if(keyCode == 37){
    cam.rotating.x+=2;  
  }
  
  if(keyCode == 39){
    cam.rotating.x-=2;
  }

  if(keyCode == 38){
    cam.moving = 1;
  }
  
  if(keyCode == 40){
    cam.moving = -1;
  }
 
  cam.update();
    console.log("Camera moved towards at "+cam.look.x+","+cam.look.z);
}

function keyReleased(){
  cam.moving = false;
  cam.rotating.x = 0;
  cam.rotating.y = 0;
}


