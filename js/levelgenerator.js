class levelgenerator{
	constructor(spritemap){
		this.boxsize = 128;
		this.spritemap = spritemap;
		
	}

	draw(level){
		for(var sprite in level){
			
			level[sprite].forEach(obj => {
				this.placeSprite(sprite, obj.x, 0, obj.z);
			})

		}
	}


	placeSprite(sprite, x, y, z){
		push();
			fill(color(255,0,0));
			translate(x*this.boxsize, y*this.boxsize, z*this.boxsize);
			texture(sprites[sprite]);
			box(this.boxsize, this.boxsize, this.boxsize);
		pop();  	
	}

}