function lookingCam(){
  this.pos = createVector(650,0,400);
  this.look = createVector(0,0,0);
  this.rotation = createVector(0,0,0);  
  this.rotating = createVector(0,0,0);

  this.moving = false;
  
  this.rotateX = function(val){
    this.rotation.x +=val;
  }
  
  this.update = function(){
      
      this.rotation.x += this.rotating.x;
      this.rotation.y += this.rotating.y;
      this.rotation.z += this.rotating.z;
  
      this.look.x = this.pos.x + Math.sin(radians(this.rotation.x))*100;
      this.look.z = this.pos.z + Math.cos(radians(this.rotation.x))*100;
   
      if(this.moving == 1){
        this.moveForward();
      }
      
      if(this.moving == -1){
        this.moveBackwards();
      }
   
      camera( 
          this.pos.x, this.pos.y, this.pos.z, 
          this.look.x, this.look.y, this.look.z, 
          0, 1, 0
      );
  }
  
  this.moveForward = function(){
      this.pos.x = this.pos.x + Math.sin(radians(this.rotation.x))*20;
      this.pos.z = this.pos.z + Math.cos(radians(this.rotation.x))*20;
  }
  
  this.moveBackwards = function(){
      this.pos.x = this.pos.x + Math.sin(radians(this.rotation.x+180))*20;
      this.pos.z = this.pos.z + Math.cos(radians(this.rotation.x+180))*20;
  }
}